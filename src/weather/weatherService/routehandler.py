import math
from geopy.distance import geodesic
import numpy as np
import json


def routeHandler(route: dict, point_sep: float = 50):
    """A function that takes a dictionary of a route and inserts intermediate points.
    route: A dictionary containing a complete valid route
    point_sep: The maximum distance between consecutive points in meters (default: 1000)

    Args:
        route (dict): A dictionary of arrays containing the route.
        point_sep (float, optional): The minimum point in meters to add intermediate points. Defaults to 50.

    Returns:
        _type_: _description_
    """

    lats = route["lats"]
    lons = route["lons"]
    alts = route["alts"]
    timestamps = route["timestamps"]

    index = 0
    "Consider two points at a time"
    while index < (len(lats)-1):
        point_A = (lats[index], lons[index])
        point_B = (lats[index+1], lons[index+1])

        distance = geodesic(point_A, point_B).m

        # If the distance is too far, insert a new point between the original two points, then continue the search from the first point, checking it against the new waypoint
        if distance > point_sep:
            mpoint = midpoint(point_A, point_B)
            route["lats"].insert(index+1, mpoint[0])
            route["lons"].insert(index+1, mpoint[1])
            route["alts"].insert(index+1, None)
            route["timestamps"].insert(index+1, None)
        else:
            # If the distance is close enough, move on to the next point
            index += 1

    # Fill in timestamp values
    for i in range(len(route["timestamps"][:-1])):
        if route["timestamps"][i] is not None and route["timestamps"][i + 1] is None:
            start = i
            subroute = None

            subsection = route["timestamps"][start+1:]

            # Find next non-null value
            for stamp in subsection:
                if stamp is not None:
                    end = subsection.index(stamp) + start+1
                    subroute = route["timestamps"][start:end+1]
                    break

            length = len(subroute)

            # For all null values inbetween start and end indeces, fill in the appropriate values
            for j in range(round(length/2)+1):
                route["timestamps"][end-j] = route["timestamps"][end]
                route["timestamps"][start+j] = route["timestamps"][start]
                if (end - j) < j:
                    break

            # Interpolate altitude values
            new_alts = np.linspace(
                route["alts"][start], route["alts"][end], length, endpoint=True, dtype=int)
            for k in range(length):
                route["alts"][k+start] = new_alts[k]

    return route


# Given two points consisting of a latitude and longitude, returns the midpoint
def midpoint(point1, point2):
    lat1, lon1 = point1
    lat2, lon2 = point2
    lat1, lon1, lat2, lon2 = map(math.radians, (lat1, lon1, lat2, lon2))

    dlon = lon2 - lon1

    dx = math.cos(lat2) * math.cos(dlon)
    dy = math.cos(lat2) * math.sin(dlon)

    lat3 = math.atan2(math.sin(lat1) + math.sin(lat2),
                      math.sqrt((math.cos(lat1) + dx) * (math.cos(lat1) + dx) + dy * dy))
    lon3 = lon1 + math.atan2(dy, math.cos(lat1) + dx)

    return (math.degrees(lat3), math.degrees(lon3))
