"""Controls access to all weather functions.
    """
import json
from typing import List

from src.weather.weatherService.routehandler import routeHandler
from src.weather.weatherService.wxServer import server_request


def wx_requester(lats: List[float], lons: List[float], alts: List[float], timestamps: List[str], certificate: str = '', key: str = ''):
    """Requests weather data from the wxserver.

    Args:
        lats (List[float]): _description_
        lons (List[float]): _description_
        alts (List[float]): _description_
        timestamps (List[str]): _description_
        certificate (str, optional): _description_. Defaults to ''.
        key (str, optional): _description_. Defaults to ''.

    Returns:
        _type_: _description_
    """
    returned = server_request(lats, lons, alts, timestamps, certificate, key)

    data = {}
    for i in range(len(lats)):
        wind_dir, wind_speed = returned[i]['wind_direction'], returned[i]['wind_speed']
        data[i] = {
            'lat': lats[i],
            'lon': lons[i],
            'alt': alts[i],
            'timestamp': timestamps[i],
            'wind_dir': wind_dir,
            'wind_speed': wind_speed
        }

    return data


def get_weather(route: dict, weather_path: str = 'out/gen_weather.json') -> (str, dict):
    """Get the weather for a given route. Also save the weather to a file and return
    the path to the file.

    Args:
        route (dict): The route dictionary.

    Returns:
    str, dict: Returns the string path to the route as well as the dictionary of weather.
    """

    # First update the route to include intermediate points for weather interpolation
    new_route = routeHandler(route)

    data = request_weather(
        new_route["lats"], new_route["lons"], new_route["alts"], new_route["timestamps"])

    try:
        with open(weather_path, 'w') as f:
            json.dump(data, f)
    except Exception as e:
        print(e)

    return weather_path, data
