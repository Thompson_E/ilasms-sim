
import math
from typing import List

import numpy as np

# ------ Defaults ------
DragCo = [0.606, 0.702, 1.192]  # Drag coefficient
Area = [0.062214, 0.0698, 0.167784]  # Area

Temp = 20  # Temperature in Celsius
Pressure = 101.325  # Temperature in kPa


class WindForce:
    def __init__(self, Cd: List[float] = None, A: List[float] = None):

        self.cd = np.array(Cd) if Cd is not None else np.array(DragCo)
        self.A = np.array(A) if A is not None else np.array(Area)

        self.kB = 1.38064852e-23
        self.mD = 4.81e-26

    def get_drag(self, v: List[float], wv: List[float], rho: float = 1.225):
        v, wv = np.array(v), np.array(wv)

        a = self.cd*self.A

        relv = (v - wv) ** 2

        return 0.5*(rho*relv*a)

    def get_rhoT(self, T, P):
        "Get rho based on Temperature in C and pressure in kPa"

        RSpecific = self.kB/self.mD

        T = T + 273.15  # Convert to Kelvin
        P = P*1000  # Convert to Pa
        top = P*self.mD
        bottom = self.kB*T

        return top/bottom


def interpolate_wind(ac_pos, wind_states, positions):
    first = wind_states[0]
    second = wind_states[1]

    # Distance from first to AC
    ac_dist = math.sqrt(((positions[0][0]-ac_pos[0])**2) +
                        ((positions[0][1]-ac_pos[1])**0))
    # Distance from first to second
    trav_dist = math.sqrt(((positions[0][0]-positions[1][0])**2) +
                          ((positions[0][1]-positions[1][1])**0))

    # Get the alpha for COSINE interpolation
    alpha = ac_dist/trav_dist

    # Convert from heading to standard angle
    angle1 = 90-first[1]
    angle1 = angle1 % 360

    angle2 = 90-second[1]
    angle2 = angle2 % 360

    # Given the two weather points, convert from heading and speed to u and v vectors
    u1 = first[0] * math.cos(math.radians(angle1))
    v1 = first[0] * math.sin(math.radians(angle1))
    u2 = second[0] * math.cos(math.radians(angle2))
    v2 = second[0] * math.sin(math.radians(angle2))

    # Convert data into a u component and v component
    mu = (1-math.cos(alpha * math.pi))/2
    u = u1 * (1-mu) + u2 * mu
    v = v1 * (1-mu) + v2 * mu

    # Returns u,v,z in m/s
    return [u, v, 0]
