import math

import numpy as np

from src.airframe import Airframe
from src.allocation import Allocator
from src.battery.tarotWrapper import TarotBattery
from src.controller.altitudeController import AltitudeController
from src.controller.attitudeController import AttitudeController
from src.controller.positionController import PositionController
from src.motor.motor import Motor


class GlobalSim:
    def __init__(self, n_motors: int = 8, ignore_battery: bool = False, output_folder: str = 'out',
                 out_filename: str = 'test_out'):

        self.n_motors = n_motors
        self.ignore_battery = ignore_battery
        self.output_folder = output_folder
        self.output_filename = out_filename

        self.i = 0

        self.init_aircraft(n_motors)

    def init_aircraft(self, n_motors: int):
        """Initilise the aircraft and controllers.
        """
        MotorParams = {"torqueConst": 0.0265, "equivResistance": 0.2700, "currentSat": 38,
                       "staticFric": 0, "damping": 0, "J": 5.0e-5, "thrustCoeff": 0.065}
        icMotor = 0
        self.sampleTime = 0.01
        self.motorArray = []

        for _ in range(self.n_motors):
            m = Motor(MotorParams, icMotor, self.sampleTime)
            self.motorArray.append(m)

        # Create three batteries
        self.bat = TarotBattery(
            self.sampleTime, f'{self.output_folder}/{self.output_filename}-battery.txt')

        # Initialize Airframe
        TarotParams = {"g": 9.80, "m": 10.66, "l": 0.6350, "b": 9.8419e-05, "d": 1.8503e-06, "minAng": math.cos(
            math.pi/self.n_motors), "maxAng": math.cos(3*math.pi/self.n_motors), "Ixx": 0.2506, "Iyy": 0.2506, "Izz": 0.4538, "maxSpeed": 670, "voltageSat": 0.0325}

        # Initialize Control Allocation
        self.allocator = Allocator(TarotParams)

        # Initial controllers
        AltitudeControllerParams = {
            'm': 10.66, 'g': 9.8, 'kdz': -1, 'kpz': -0.5}

        PositionControllerParams = {'kpx': 0.1, 'kdx': 0, 'kpy': 0.1,
                                    'kdy': 0, 'min_angle': -12*math.pi/180, 'max_angle': 12*math.pi/180}

        AttitudeControllerParams = {"kdphi": 1, "kpphi": 3,
                                    "kdpsi": 1, "kppsi": 3, "kdtheta": 1, "kptheta": 3}

        self.tarot = Airframe(TarotParams, self.sampleTime)
        self.altitudeController = AltitudeController(AltitudeControllerParams)
        self.attitudeController = AttitudeController(AttitudeControllerParams)
        self.positionController = PositionController(PositionControllerParams)

    def step(self, refrence, windforces):

        psiRef = 0
        # Prepare data for control
        xref, yref, zref = refrence[0], refrence[1], refrence[2]
        state = self.tarot.getState()

        # Get alt fz
        fz = self.altitudeController.output(state, zref)
        # Get theta and phi references
        thetaRef, phiRef = self.positionController.output(state, [xref, yref])
        # Get roll, pitch and yaw
        roll, pitch, yaw = self.attitudeController.output(
            state, [phiRef, thetaRef, psiRef])

        uDesired = [fz, roll, pitch, yaw]

        # Motor/Battery
        refVoltage = self.allocator.getRefVoltage(uDesired)
        # Iterate over each motor
        rpm = np.zeros(self.n_motors, dtype=np.float32)

        totalCurrent = 0

        # Update total current
        for idx, motor in enumerate(self.motorArray):
            rpm[idx], current = motor.getAngularSpeed(refVoltage[idx])
            totalCurrent += current

        # ! If ignore_battery is true then the battery params arent updated (ignored by default).
        if not self.ignore_battery:
            try:
                output = self.bat.step({"i": totalCurrent})

                if output < 0:
                    raise ValueError()
            except ValueError as e:
                print("::WARNING:: Simulation exiting. Battery has fallen below 0.")
                raise ValueError("Battery has fallen below 0.") from e

        state = self.tarot.update(rpm, windforces)

        # Add noise to the state
        # if self.i % 100 == 0:
        #     state[0] += np.random.normal(0, 0.2)
        #     state[1] += np.random.normal(0, 0.2)

        self.i += 1

        return state
