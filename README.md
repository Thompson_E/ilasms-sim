# ILASMS Sim
This is a repository utilising the T18 python simulation from Luke Bhan [https://github.com/lukebhan/TarotT18](https://github.com/lukebhan/TarotT18) with a project specific wrapper.

## Running the code
To run the code use `python main.py` along with any accompanying flags:
#### Flags
* `-route`: This is the path to the file containing the route (Not required)
* `-weather`: This is the path to the file containing the weather params. If the file does not exist then weather will be retrieved seperately and stored under this filename (Not required)
* `-ignoreBattery`: This is a boolean flag to ignore the battery state during the simulation (Defaults to False)

## Trajectory file
The trajectory file should be uploaded and parsed from a JSON file in either of the following formats:

1.
```json
{
  "name of trajectory":{
    "lats":[lat1, lat2, ..., latn],
    "lons":[lon1, lon2, ..., lonn],
    "alts":[alt1, alt2, ..., altn],
    "timestamps":["yyyy-mm-ddThh:mm:ss+xx:xx", "yyyy-mm-ddThh:mm:ss+xx:xx", ..., "yyyy-mm-ddThh:mm:ss+xx:xx"]
  }
}
```

2.
```json
{
  "name of trajectory":[
    {"lat":lat1, "lon":lon1, "alt":alt1, "timestamp":"yyyy-mm-ddThh:mm:ss+xx:xx"},
    {"lat":lat2, "lon":lon2, "alt":alt2, "timestamp":"yyyy-mm-ddThh:mm:ss+xx:xx"},
    ...,
    {"lat":latn, "lon":lonn, "alt":altn, "timestamp":"yyyy-mm-ddThh:mm:ss+xx:xx"},
  ]
}
```