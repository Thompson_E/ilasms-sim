import argparse
import json
from math import pi, sqrt

from tqdm import tqdm

import geopy
import matplotlib.pyplot as plt
from geographiclib.geodesic import Geodesic as Gdisc
from geopy.distance import geodesic

from src.globalSim import GlobalSim
from src.weather.loopFunctions import *
from src.weather.weathermain import *

DEFAULT_ROUTE = "in/routes/defaultroute.json"
DEFAULT_WEATHER = "in/weather/defaultweather.json"

# Todo: Test weather generation
# Todo: Run the simulation


def main(args):
    """The main body of the program. Here all other features and functions are controlled and called.

    Args:
        args(object): A dictionary of arguments
    """

    # Get the route and weather
    print("::MESSAGE:: Preparing inputs ...")
    route, weather = prepare_inputs(args.route_path, args.weather_path)
    print("::MESSAGE:: Weather and Route [Ready]")

    # Initilise main program
    print("::MESSAGE:: Initilising Global simulation ...")
    sim = GlobalSim(ignore_battery=bool(args.ignoreBattery))
    print("::MESSAGE:: Global simulation [Ready]")

    print("::MESSAGE:: Initilising Wind Force Calculator ...")
    wind_forces = WindForce()
    print("::MESSAGE:: Wind Force Calculator [Ready]")

    main_loop(route, weather, sim, wind_forces)


def prepare_inputs(route_path: str, weather_path: str) -> (dict, dict):
    """Prepares the input path for route and weather. If the path is not given, then the default

    Args:
        route_path (str): A path to the route file.
        weather_path (str): A path to the weather file.

    Returns:
        dict, dict: The route and weather dictionaries.
    """

    # Get the route
    route_path = route_path or DEFAULT_ROUTE

    with open(route_path, "r") as f:
        route = json.load(f)

    weather_path = weather_path or DEFAULT_WEATHER

    route = route["route"]

    route = [[route["x"][i], route["y"][i], 50] for i in range(len(route["x"]))]

    with open(weather_path, "r") as f:
        weather = json.load(f)["route"]

    assert len(route) == len(weather)

    return (route, weather)


def main_loop(route, weather, sim: GlobalSim, wind_forces):
    """The main loop of the system

    Args:
        route (_type_): _description_
        weather (_type_): _description_
        sim (_type_): _description_
    """

    # Todo: Implement main loop -- In progress

    # plt.ion()

    current_index = 1
    ac_pos = route[0]

    true_x, true_y = [ac_pos[0]], [ac_pos[1]]

    figure, ax = plt.subplots()

    (line1,) = ax.plot(true_x, true_y)
    scat1 = ax.scatter(np.array(route)[:, 0], np.array(route)[:, 1])

    v = [0, 0, 0]

    print(len(route), len(weather))

    with tqdm(total=len(route)) as pbar:

        while current_index != len(route):

            # Get current wind
            observed_wind = interpolate_wind(
                ac_pos,
                weather[current_index - 1 : current_index + 1],
                route[current_index - 1 : current_index + 1],
            )

            observed_wind = [0, 0, 0]

            # Get wind force
            w_force = wind_forces.get_drag(v, observed_wind)

            # Update the simulation, catch battery issue if it occurs
            try:
                state = sim.step(refrence=route[current_index], windforces=w_force)
            except ValueError:
                break

            # Check if the drone has reached the next waypoint

            x_error = abs(state[0] - route[current_index][0])
            y_error = abs(state[1] - route[current_index][1])

            v = [(state[0] - ac_pos[0]) / 0.01, (state[1] - ac_pos[1]) / 0.01, 0]
            ac_pos = state[:4]

            true_x.append(state[0])
            true_y.append(state[1])

            # line1.set_xdata(true_x)
            # line1.set_ydata(true_y)

            # figure.canvas.draw()
            # figure.canvas.flush_events()

            if x_error <= 0.5 and y_error <= 0.5:
                current_index += 1
                pbar.update(1)

    plt.plot(true_x, true_y)
    plt.scatter(np.array(route)[:, 0], np.array(route)[:, 1])
    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    # ------ ARGUMENTS ------
    # Path to route file
    parser.add_argument(
        "-route", dest="route_path", default=None, help="Path to route file"
    )

    # Path to weather file
    parser.add_argument(
        "-weather", dest="weather_path", default=None, help="Path to weather file"
    )

    # Ignore battery
    parser.add_argument(
        "-ignoreBattery",
        action="store_true",
        help="Should the batterybe ignored in the simulation (default: False)",
    )
    args = parser.parse_args()

    # TODO: Add input for Cd, A, Temp and Pressure at some point.

    # Run the main function
    main(args)
